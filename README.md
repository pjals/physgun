# Physics Gun

Adds a simple "Physics Gun" to Minetest, this can currently only hold entities but later nodes too!

## Usage

Give yourself the Physics Gun (`/give username physgun:pg`) and spawn/find an object such as a boat or cow, and press the use (left click) key on it to hold it, press the same key to hold it.

## License

License is AGPLv3.0 or later, more info found at `LICENSE.md`
