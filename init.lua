--[[
    physgun - Simple Physics Gun - https://git.envs.net/pjals/physgun
    Copyright (C) 2022-2022 pjals (Daniel [REDACTED])

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local mulv = vector.multiply

local function send(u,...)
  minetest.chat_send_player(u, table.concat({...}, " "))
end

local EYE_OFFSET = {
  x=0,
  y=1.625,
  z=0
}

local holding = {}

minetest.register_privilege("physgun", {description = "Allow using Physics Gun"})
minetest.register_privilege("physgun_player", {description = "Allow using Physics Gun to hold players."})


minetest.register_tool("physgun:pg", {
  on_use = function(_,u,_)
    local name = u:get_player_name()
    if not minetest.get_player_privs(name)["physgun"] then
      send(name, "Error: Not enough privileges to use Physics Gun (grant physgun).")
      return
    end
    if holding[name] then
      send(name, "Unholding...")
      local ref = holding[name].ref
      if ref:is_player() then
        playerphysics.remove_physics_factor(ref, "speed", "physgun", 0)
        playerphysics.remove_physics_factor(ref, "jump", "physgun", 0)
        playerphysics.remove_physics_factor(ref, "gravity", "physgun", 0)
      else
        ref:add_velocity(holding[name].old_velocity)
      end
      holding[name] = nil
      return
    end
    local dir = u:get_look_dir()
    local endr = mulv(dir, 10)
    local eyepos = u:get_pos() + EYE_OFFSET + u:get_eye_offset()
    local rc = minetest.raycast(
      eyepos,
      eyepos + endr,
      true,
      false
    )
    for p in rc do
      if p.type == "object" and p.ref:get_player_name() ~= name then
        send(name, "Holding: ", p.ref:get_player_name())
        p.old_velocity = p.ref:get_velocity()
        if p.ref:is_player() then
          if not minetest.get_player_privs(name)["physgun_player"] then
            send(name, "Error: Not enough privileges to use Physics Gun to hold players. (grant physgun_player)")
            return
          end
          playerphysics.add_physics_factor(p.ref, "speed", "physgun", 0)
          playerphysics.add_physics_factor(p.ref, "jump", "physgun", 0)
          playerphysics.add_physics_factor(p.ref, "gravity", "physgun", 0)
        end
        
        holding[name] = p
        break
      end
    end
  end,
  on_secondary_use = function(_,u,_)
    local name = u:get_player_name()
    if not holding[name] then return end
    local ref = holding[name].ref
    -- Rotation logging is annoying, uncomment if you want it.
    -- send(name, "Rotating..")
    
    if ref:is_player() then
      ref:set_look_horizontal(ref:get_look_horizontal() + (math.pi/8))
    else
      ref:set_yaw(ref:get_yaw() + (math.pi/8))
    end
  end
})

minetest.register_globalstep(function()
  for player,obj in pairs(holding) do
    local pref = minetest.get_player_by_name(player)
    if not obj.ref:is_player() then
      obj.ref:set_velocity({x=0,y=0,z=0})
    end
    obj.ref:set_pos(
      pref:get_pos() + EYE_OFFSET + pref:get_eye_offset() +
        {x=0, y=-(obj.ref:get_properties()["selectionbox"][5]/2), z=0} +
        mulv(pref:get_look_dir(), 4)
    )
  end
end)
